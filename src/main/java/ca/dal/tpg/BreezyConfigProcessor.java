package ca.dal.tpg;

import ca.dal.tpg.exceptions.AddonFolderNotFound;
import io.vertx.config.spi.ConfigProcessor;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.Future;
import io.vertx.reactivex.core.Promise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.time.Instant;

/** The Breezy Config Processor holds changes to the live configuration, made by
 *  users through sentry. When the config is retrieved, elsewhere on the server
 *  it merges these changes into the configurations before they are returned
 *  to the caller.
 */
public class BreezyConfigProcessor implements ConfigProcessor {

    private static final Logger log = LoggerFactory.getLogger(BreezyConfigProcessor.class);
    private JsonObject sentryConfig = new JsonObject();
    private Instant sentryJsonUpdateTimestamp = Instant.now();
    private Instant configFileUpdateTimestamp = Instant.now();
    private static final String CONFIG_PATH = "conf.json";
    public static final String ADDON_PATH = "game\\dota_addons\\";
    private static final String WORKSHOP_ID = "1976046474";

    public void setSentryConfig(JsonObject config){
        this.sentryConfig = config;
    }


    public void updateSentryConfig(String key, String value){
        sentryConfig.put(key, value);
        sentryJsonUpdateTimestamp = Instant.now();
    }

    public void updateSentryConfig(String key, boolean value){
        sentryConfig.put(key, value);
        sentryJsonUpdateTimestamp = Instant.now();
    }

    public void updateSentryConfig(String key, JsonObject value){
        sentryConfig.put(key,value);
        sentryJsonUpdateTimestamp = Instant.now();
    }

    @Override
    public String name() {
        return "Sentry Config";
    }

    //This method is not actually used, but is required to implement the ConfigProcessor interface
    @Override
    public void process(Vertx vertx, JsonObject configuration, Buffer input, Handler<AsyncResult<JsonObject>> handler) {

    }

    public JsonObject process(JsonObject object) {

        if(sentryConfigUpdateTimestamp() > configFileModifiedTimestamp()){

            object.mergeIn(sentryConfig);
        }else{


            //Inherit any changes to conf.json that happened to fields that
            //can be modified through sentry.
            object.forEach(e->{
                if(sentryConfig.containsKey(e.getKey())){
                    sentryConfig.put(e.getKey(), e.getValue());
                }
            });

        }
        return object;
    }

    public long sentryConfigUpdateTimestamp(){
        return sentryJsonUpdateTimestamp.toEpochMilli();
    }

    public long configFileModifiedTimestamp(){
        File configFile = new File(CONFIG_PATH);
        return configFile.lastModified();
    }

    /** Looks for the addon folder specified as the addon id
     *  if the user isn't using the workshop version.
     *
     * @param addonId
     * @return
     */
    public Future<Void> validateAddonId(String addonId){
        Promise<Void> promise = Promise.promise();
        if(addonId.equals(WORKSHOP_ID)){ //Using workshop id, done!
            promise.complete();
            return promise.future();
        }

        //Otherwise check to make sure the addon id refers to a folder in the right directory.
        Server.getInstance().configRetriever().rxGetConfig().subscribe(
                config->{

                    File addonFolder = new File(
                            config.getString("dotaBetaFolderPath") +
                                    ADDON_PATH +
                                    addonId
                    );

                    if(addonFolder.exists() && addonFolder.isDirectory()){
                        promise.complete();
                    }else{
                        promise.fail(new AddonFolderNotFound());
                    }

                }
        );

        return promise.future();
    }
}
