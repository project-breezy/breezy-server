package ca.dal.tpg.exceptions;

public class AddonFolderNotFound extends Exception {

    public String getMessage(){
        return "The addon id could not be used to find the addon folder! Either the id is incorrect, or the addon folder wasn't placed on the correct path.";
    }

}
