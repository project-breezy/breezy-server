package ca.dal.tpg.exceptions;

/** This is a wrapper exception that simply wraps a string message
 *  from an old exception. It is what exceptions get loaded as
 *  from the database when a game exception is stored in the record.
 */
public class LoadedException extends Exception {

    private String msg;

    public LoadedException (String msg){
        this.msg = msg;
    }

    public String getMessage(){
        return msg;
    }
}
