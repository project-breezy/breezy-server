package ca.dal.tpg.exceptions;

import ca.dal.tpg.model.Run;
import ca.dal.tpg.model.RunStatus;

public class FailedToStartDota2 extends Exception {

    private Run run;
    private String message;
    private Exception cause;

    public FailedToStartDota2(String message, Exception cause){
        this.message = message;
        this.cause = cause;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public Run getRun(){
        return run;
    }

    public Exception getCause(){
        return cause;
    }
}
