package ca.dal.tpg.exceptions;

public class DotaProcessCrash extends Exception {

    private int exitValue;

    public DotaProcessCrash(int exitValue){
        this.exitValue = exitValue;
    }

    public int getExitValue(){
        return exitValue;
    }

    public String getMessage(){
        return "Dota process crashed!";
    }

}
