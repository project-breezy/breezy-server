package ca.dal.tpg;


import ca.dal.tpg.exceptions.AddonFolderNotFound;
import ca.dal.tpg.model.*;

import io.vertx.config.ConfigChange;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;

import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerOptions;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.bridge.PermittedOptions;

import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandlerOptions;
import io.vertx.reactivex.config.ConfigRetriever;
import io.vertx.reactivex.core.AbstractVerticle;

import io.vertx.reactivex.core.Future;
import io.vertx.reactivex.core.Promise;
import io.vertx.reactivex.core.Vertx;
import io.vertx.reactivex.core.buffer.Buffer;
import io.vertx.reactivex.core.eventbus.EventBus;
import io.vertx.reactivex.core.http.HttpServer;

import io.vertx.reactivex.ext.web.Route;
import io.vertx.reactivex.ext.web.Router;
import io.vertx.reactivex.ext.web.RoutingContext;
import io.vertx.reactivex.ext.web.handler.BodyHandler;
import io.vertx.reactivex.ext.web.handler.StaticHandler;
import io.vertx.reactivex.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.reactivex.ext.web.handler.sockjs.SockJSSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.awt.*;
import java.io.File;
import java.util.*;

public class Server extends AbstractVerticle {

    private static final Logger log = LoggerFactory.getLogger(Server.class);

    private static Server instance;

    private HttpServer server;
    private Router router;
    private EventBus eb;
    private ConfigRetriever retriever;
    private BreezyConfigProcessor configProcessor = new BreezyConfigProcessor();
    private AgentClient agentClient;
    private Route gameHandler;

    private Store store = null;
    private int actionCode = 1;

    private SockJSSocket featureSocket;

    public static Server getInstance(){
        if(instance != null){
            return instance;
        }else {
            log.error("Attempted to get server singleton before init.");
            return null;
        }
    }




    //Start hook
    public void start(){

        setupConfigHotReload();

        log.info("Starting with config: \n{}", config().encodePrettily());

        //Get the event bus
        this.eb = vertx.eventBus();

        //Load JDBC config and create database client
        agentClient = new AgentClient(vertx, config().getJsonObject("agent"));
        store = new Store(vertx, config().getJsonObject("jdbc"));

        //Setup and create http server
        HttpServerOptions options = new HttpServerOptions()
                .setPort(config().getJsonObject("httpServer").getInteger("port"))
                .setHost(config().getJsonObject("httpServer").getString("host"));

        server = vertx.createHttpServer(options);

        router = Router.router(vertx);
        router.route().handler(BodyHandler.create());

        //Set up SockJS Handler
        BridgeOptions opts = new BridgeOptions()
                .addInboundPermitted( new PermittedOptions()
                    .setAddress("sentry.features"))
                .addInboundPermitted( new PermittedOptions()
                    .setAddress("sentry.latency"))
                .addInboundPermitted( new PermittedOptions()
                    .setAddress("sentry.activeRun"))
                .addOutboundPermitted( new PermittedOptions()
                    .setAddress("sentry.features"))
                .addOutboundPermitted( new PermittedOptions()
                    .setAddress("sentry.latency"))
                .addOutboundPermitted( new PermittedOptions()
                    .setAddress("sentry.activeRun"));

        SockJSHandler handler = SockJSHandler.create(vertx);
        SockJSHandlerOptions sockJsOptions = new SockJSHandlerOptions().setHeartbeatInterval(2000);
        SockJSHandler socketHandler = SockJSHandler.create(vertx, sockJsOptions);
        socketHandler.socketHandler(socket->{
            this.featureSocket = socket;
        });
        router.mountSubRouter("/eventbus", handler.bridge(opts));


        router.route().handler(this::handleCors);

        router.route().method(HttpMethod.OPTIONS).handler(this::handleOptions);
        router.route("/stream/features/*").handler(socketHandler);
        router.route(HttpMethod.POST,"/dota2/").handler(this::gameClientHandler);
        router.route(HttpMethod.GET, "/action").handler(this::handleActionCode);
        router.route(HttpMethod.GET, "/features/").handler(this::handleFeatures);
        router.route(HttpMethod.GET, "/state/").handler(this::handleServerState);
        router.route(HttpMethod.POST, "/state/").handler(this::handleNewServerState);
        router.route(HttpMethod.GET, "/agent").handler(this::setAgentOverride);
        router.route(HttpMethod.POST, "/agent/config").handler(this::updateAgentConfig);
        router.route(HttpMethod.GET, "/agent/config").handler(this::getAgentConfig);
        router.route(HttpMethod.POST, "/agent/config/test").handler(this::testAgentConfig);
        router.route(HttpMethod.GET, "/utility/openAddonFolder").handler(this::openAddonFolder);

        //Run Management Routes
        router.route(HttpMethod.POST, "/run/").handler(Run::createRun);
        gameHandler  = router.route(HttpMethod.POST, "/run/dota2").handler(Run::dota2GameHandler);
        router.route(HttpMethod.POST, "/run/client").handler(Run::dota2ClientHandler);
        router.route(HttpMethod.GET, "/run/active").handler(Run::activeRunInfoHandler);
        router.route(HttpMethod.DELETE, "/run/active").handler(Run::cancelActiveRunHandler);
        router.route(HttpMethod.GET, "/run/:runId").handler(Run::getRunInfoHandler);
        router.route(HttpMethod.GET, "/run/").handler(Run::getAllRunsHandler);

        //Serve UI
        router.route("/*").handler(StaticHandler.create());

        server.requestHandler(router).rxListen().subscribe(
                success->{
                    log.info("Server started successfully at {} on port {}", config().getJsonObject("httpServer").getString("host"), config().getJsonObject("httpServer").getInteger("port"));
                },
                err->{
                    log.error("Error starting server at {} on port {}", config().getJsonObject("httpServer").getString("host"), config().getJsonObject("httpServer").getInteger("port"));
                    log.error(err.getMessage());
                    err.printStackTrace();
                }
        );

        setupFeatureStreaming();

        instance = this;

    }

    private void setupConfigHotReload() {
        //Set up config retriever
        ConfigStoreOptions file = new ConfigStoreOptions()
                .setType("file")
                .setFormat("json")
                .setConfig(new JsonObject().put("path","conf.json"));

        ConfigRetrieverOptions configOptions = new ConfigRetrieverOptions()
                .setScanPeriod(3000)
                .addStore(file);
        retriever = ConfigRetriever.create(vertx, configOptions);
        retriever.setConfigurationProcessor(configProcessor::process);

        retriever.listen(change->{
            try {

                log.info("Change!");

                //Restart HTTP server if server settings have changed
                JsonObject newServerConfig = change.getNewConfiguration().getJsonObject("httpServer");
                JsonObject oldServerConfig = change.getPreviousConfiguration().getJsonObject("httpServer");
                if (!newServerConfig.encode().equals(oldServerConfig.encode())) {
                    HttpServerOptions httpOptions = new HttpServerOptions()
                            .setPort(newServerConfig.getInteger("port"))
                            .setHost(newServerConfig.getString("host"));
                    HttpServer oldServer = server;
                    server = vertx.createHttpServer(httpOptions);
                    oldServer.rxClose().andThen(server.requestHandler(router).rxListen()).subscribe(
                            success -> log.info("Http Server successfully restarted! {}:{}",
                                    newServerConfig.getString("host"),
                                    newServerConfig.getInteger("port")),
                            err -> log.error("Http Server failed to restart! {}", err.getMessage())
                    );

                }

                //Replace Agent Client if agent settings have changed
                JsonObject newAgentConfig = change.getNewConfiguration().getJsonObject("agent");
                JsonObject oldAgentConfig = change.getPreviousConfiguration().getJsonObject("agent");
                if (!oldAgentConfig.encode().equals(oldAgentConfig.encode())
                ) {
                    agentClient = new AgentClient(vertx, newAgentConfig);
                    printConfigChange(change, "agent", JsonObject.class);
                }

                //Replace Store if JDBC settings have changed
                JsonObject newJdbc = new JsonObject();
                JsonObject oldJdbc = new JsonObject();
                if (!newJdbc.encode().equals(oldJdbc.encode())
                ) {
                    store = new Store(vertx, newJdbc);
                    printConfigChange(change, "jdbc", JsonObject.class);
                }

                printConfigChange(change, "dotaBetaFolderPath", String.class);

                printConfigChange(change, "overrideAgent", Boolean.class);

                printConfigChange(change, "bypass.agent.ready.between.games", Boolean.class);

                printConfigChange(change, "workshop_addon_id", String.class);

                printConfigChange(change, "run.webhook.prefix", String.class);

                printConfigChange(change, "run.webhook.suffix", String.class);

                getServerState().onSuccess(
                        serverState->eb.publish("sentry.latency", serverState)
                );

            }catch (NullPointerException npe){
                if(change.getPreviousConfiguration().isEmpty()){
                    log.info("Detected first config");
                }else{
                    npe.printStackTrace();
                }
            }
        });

    }

    private void printConfigChange(ConfigChange change, String key, Class type){
        switch (type.getSimpleName()){
            case "String":
                if(!change.getPreviousConfiguration().getString(key).equals(
                        change.getNewConfiguration().getString(key)
                )){
                    log.info("{}: {} -> {}",
                            key,
                            change.getPreviousConfiguration().getString(key),
                            change.getNewConfiguration().getString(key)
                    );
                }

                break;
            case "Boolean":
                if(!change.getPreviousConfiguration().getBoolean(key).equals(
                        change.getNewConfiguration().getBoolean(key)
                )){
                    log.info("{}: {} -> {}",
                            key,
                            change.getPreviousConfiguration().getBoolean(key),
                            change.getNewConfiguration().getBoolean(key)
                    );
                }
                break;
            case "JsonObject":
                if(!change.getPreviousConfiguration().getJsonObject(key).encode().equals(
                        change.getNewConfiguration().getJsonObject(key).encode()
                )){
                    log.info("{}:\n Old:{}\nNew:{}",
                            key,
                            change.getPreviousConfiguration().getJsonObject(key).encodePrettily(),
                            change.getNewConfiguration().getJsonObject(key).encodePrettily()
                    );
                }
        }
    }

    private void setupFeatureStreaming(){

        vertx.setPeriodic(config().getLong("feature.stream.delay"), t->{
            //Write on separate thread
            vertx.rxExecuteBlocking(write->{
                if(Run.getActiveRun() == null){ //If there is no telemetry, check back later.
                    return;
                }
                JsonArray payload = Run.getActiveRun().telemetry().readFeatureVector();
                if(payload != null && featureSocket != null){

                        featureSocket.rxWrite(new Buffer(payload.toBuffer()))
                            .subscribe();
                    }
                }
            ,false).subscribe();

        });

    }

    /** Handle POSTs sent from the dota 2 client!
     *
     * @param rc Routing context of the request
     */
    private void gameClientHandler(RoutingContext rc){


        JsonObject features = rc.getBodyAsJson();
        JsonArray featuresArray = features.getJsonArray("features");
        String gameId = features.getString("gameId");
        String sequenceNo = features.getString("sequenceNo");
        Double rtt = Double.parseDouble(features.getString("latency"));

        log.info("latency: {}", rtt);

        Run.getActiveRun().telemetry().addAddonRtt(rtt);

        Features f = Features.fromJsonArray(gameId,Integer.parseInt(sequenceNo), featuresArray);

        rc.data().put("features", f);


        /* Run async - save features to database and send them out in real
         * time through websockets to the front end client.*/
        vertx.rxExecuteBlocking(db->{

            log.info(features.encodePrettily());

            log.info("features length: {}", featuresArray.size());

            store.insertFeatures(f);
            log.info("Saved features!");

        }).subscribe();

        //Reroute request to the active run's game handler

        rc.reroute(HttpMethod.POST, "/run/dota2");
    }

    private void setAgentOverride(RoutingContext rc){
        configProcessor.updateSentryConfig("overrideAgent",Boolean.parseBoolean(rc.request().getParam("override")));
        getServerState().onSuccess(serverState->
                rc.response().setStatusCode(200).end(serverState.encode()));
    }

    public AgentClient getAgentClient(){
        return agentClient;
    }

    private void getAgentConfig(RoutingContext rc){
        //Use the retriever to get the latest config
        retriever.rxGetConfig().subscribe(
                config->{
                    JsonObject response = config.getJsonObject("agent");
                    response.put("connected", agentClient.isConnected());

                    rc.response()
                            .setStatusCode(200)
                            .end(response.encode());
                }
        );

    }

    private void handleActionCode(RoutingContext rc){

        //If we were passed a value parameter
        if(rc.queryParams().contains("value"))
        { //Infer this to mean that we want to set the action code
            int newCode = Integer.parseInt(rc.queryParams().get("value"));
            this.actionCode = newCode;

            getServerState().onSuccess(serverState->
                    rc.response().setStatusCode(200).end(serverState.encode()));
        }
        else //If no value was passed, return all action codes
        {
            Action[] actions = Action.values();
            JsonArray result = new JsonArray();
            Arrays.stream(actions).forEach(action -> {
                result.add(new JsonObject()
                    .put("name", action.friendlyName())
                    .put("action", action.name())
                    .put("code", action.code())
                );
            });

            rc.response()
                    .setStatusCode(200)
                    .end(result.encode());
        }



    }

    private void testAgentConfig(RoutingContext rc){
        try{
            agentClient.testConfig(rc.getBodyAsJson()).future().onSuccess(
                    response->{
                        rc.response().setStatusCode(200).end(response.encode());
                    }).onFailure(
                        err->{
                            rc.response().setStatusCode(500).end(new JsonObject().encode());
                        }
            );
        }catch (Exception e){
            log.error(e.getMessage());
            e.printStackTrace();
        }

    }

    private void updateAgentConfig(RoutingContext rc){
        JsonObject agentConfig = rc.getBodyAsJson();
        configProcessor.updateSentryConfig("agent", agentConfig);
        agentClient.setConfig(agentConfig).future().onSuccess(
                result->{
                    JsonObject response = agentConfig.copy();
                    response.put("connected", result);
                    rc.response()
                            .setStatusCode(200)
                            .end(response.encode());
                }).onFailure(
                err->{
                    rc.response().setStatusCode(500).end(new JsonObject().encode());
                }
        );
    }

    private void handleServerState(RoutingContext rc){
        getServerState().onSuccess(serverState->
                rc.response().setStatusCode(200).end(serverState.encode()));
    }

    private void handleNewServerState(RoutingContext rc){
        JsonObject data = rc.getBodyAsJson();


        if(data.containsKey("dotaBetaFolderPath")){
            configProcessor.updateSentryConfig("dotaBetaFolderPath",
                    data.getString("dotaBetaFolderPath"));
        }

        if(data.containsKey("bypassAgent")){
            configProcessor.updateSentryConfig("bypass.agent.ready.between.games", data.getBoolean("bypassAgent"));
        }

        if(data.containsKey("actionCode")){
            this.actionCode = data.getInteger("actionCode");
        }

        if(data.containsKey("overrideAgent")){
            configProcessor.updateSentryConfig("overrideAgent", data.getBoolean("overrideAgent"));
        }

        if(data.containsKey("workshopAddonId")){

            configProcessor.validateAddonId(data.getString("workshopAddonId")).onSuccess(
                    valid->{
                        configProcessor.updateSentryConfig("workshop_addon_id", data.getString("workshopAddonId"));
                        getServerState().onSuccess(serverState->rc.response().setStatusCode(200).end(serverState.encode()));
                    }
            ).onFailure(
                    err->{
                        if(err instanceof AddonFolderNotFound){
                            //Invalid addon id or addon not placed in the right folder
                            rc.response().setStatusCode(400).end(new JsonObject().put("error", err.getMessage()).encode());
                        }else{
                            //Unexpected error
                            rc.response().setStatusCode(500).end();
                        }
                    }
            );
        }


    }

    private void handleCors(RoutingContext rc){
        rc.response()
                .putHeader("Content-Type","application/json")
                .putHeader("Access-Control-Allow-Headers", "content-type")
                .putHeader("Access-Control-Allow-Methods", "*")
                .putHeader("Access-Control-Allow-Origin", "*");
        rc.next();
    }

    private void handleOptions(RoutingContext rc){
        rc.response().setStatusCode(200).end();
    }

    public Future<JsonObject> getServerState(){
        Promise<JsonObject> promise = Promise.promise();



        retriever.rxGetConfig().map(
                conf->{
                    JsonObject result = new JsonObject()
                            .put("agentConfig", conf.getJsonObject("agent"))
                            .put("overrideAgent", conf.getBoolean("overrideAgent"))
                            .put("actionCode", this.actionCode)
                            .put("bypassAgent", conf.getBoolean("bypass.agent.ready.between.games"))
                            .put("dotaBetaFolderPath", conf.getString("dotaBetaFolderPath"))
                            .put("workshopAddonId", conf.getString("workshop_addon_id"));

                    if(Run.getActiveRun() != null){
                        result.mergeIn(Run.getActiveRun().telemetry().toJson());
                    }else{
                    /* Not ideal separation of duties between
                       RunTelemetry and Server, but necessary
                       for now to maintain api backwards compatibility.
                       TODO - Fix this.
                     */
                        result
                                .put("averageClientLatency", 0.0)
                                .put("maxClientLatency", 0L)
                                .put("minClientLatency", 0L)
                                .put("minAddonRtt", 0.0)
                                .put("maxAddonRtt", 0.0)
                                .put("averageAddonRtt", 0.0)
                                .put("averageAgentLatency", 0.0)
                                .put("maxAgentLatency", 0L)
                                .put("minAgentLatency", 0L)
                                .put("addonRtt", 0)
                                .put("clientLatencyData", 0)
                                .put("agentLatencyData", 0);
                    }

                    return result;




                }
        )
                .subscribe(result->{

                    //Check addon id
                    configProcessor.validateAddonId(result.getString("workshopAddonId")).onFailure(
                            err->{
                                if(err instanceof AddonFolderNotFound){
                                    result.put("error", err.getMessage());
                                }
                            }
                    ).onComplete(done->promise.complete(result));

                });

        return promise.future();
    }

    //Return a list of features
    private void handleFeatures(RoutingContext rc){
        Feature[] features = Feature.values();
        JsonArray result = new JsonArray();
        Arrays.stream(features).forEach(
                feature -> {
                    result.add(new JsonObject()
                            .put("name", feature.friendlyName())
                            .put("index", feature.index()));
                }
                );

        rc.response()
                .setStatusCode(200)
                .end(result.encode());

    }

    /** Opens an explorer window to where the addon folder is expected to be found.
     *
     * @param rc
     */
    private void openAddonFolder(RoutingContext rc){
        configRetriever().rxGetConfig().subscribe(
                config->{
                    Desktop.getDesktop().open(new File(
                            config.getString("dotaBetaFolderPath") +
                                    BreezyConfigProcessor.ADDON_PATH
                    ));

                    rc.response().setStatusCode(200).end();
                }
        );
    }

    public Router router(){
        return router;
    }

    public int getActionCode(){
        return actionCode;
    }

    public EventBus eb(){
        return eb;
    }

    public Vertx v(){
        return vertx;
    }

    private boolean equal(JsonObject o1, JsonObject o2){
        return o1.encode().equals(o2.encode());
    }

    public ConfigRetriever configRetriever(){
        return retriever;
    }

    public void useAsyncGameHandler(){
        log.info("Switching to Async Game Handler");
        log.info("NOTE: Override agent forced to false in this mode. Overriding agents in async mode is not supported!");
        gameHandler.remove();
        gameHandler = router.route(HttpMethod.POST, "/run/dota2")
                .handler(Run::dota2AsyncGameHandler);
        //Overriding agent not supported in async relay mode
        configProcessor.updateSentryConfig("overrideAgent", false);
    }

    public void useSyncGameHandler(){
        log.info("Switching to Sync Game Handler");
        gameHandler.remove();
        gameHandler = router.route(HttpMethod.POST, "/run/dota2")
                .handler(Run::dota2GameHandler);
    }
}
