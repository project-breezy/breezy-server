package ca.dal.tpg.model;

import ca.dal.tpg.Server;
import ca.dal.tpg.exceptions.FailedToStartDota2;
import io.vertx.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/** @Author Alexandru Ianta
 *  Manages interactions with the dota process including:
 *      - starting and stopping the underlying process
 *      - crash reporting
 */
public class DotaProcess {
    private static final Logger log = LoggerFactory.getLogger(DotaProcess.class);

    //Config
    private static final String WORKSHOP_ID = "1976046474";
    private String dotaPath;
    private String addonString; //either workshop addon id or addon folder name
    private String processOutputFileName;
    private String processErrorFileName;
    private String processInputFileName;
    private Thread processMonitor;

    private Process process;

    public boolean isAlive(){
        return process != null ? process.isAlive():false;
    }

    public boolean usingWorkshopAddon(){
        return addonString.equals(WORKSHOP_ID)?true:false;
    }

    public void restartDotaProcess() throws FailedToStartDota2 {
        destroyDotaProcess();
        createDotaProcess();
    }

    public void destroyDotaProcess(){
        if (process != null){
            try{
                process.destroyForcibly().waitFor();
            }catch (InterruptedException e){
                log.error("Interrupted while destroying dota process!");
                e.printStackTrace();
            }

        }
    }

    public void createDotaProcess() throws FailedToStartDota2 {
        try{

            //Create dota 2 process
            ProcessBuilder pb = new ProcessBuilder(
                    dotaPath + "game\\bin\\win64\\dota2.exe",
                    "-override_vpk",
                    "-addon",
                    addonString,
                    "-tools",
                    "-novid",
                    "-condebug",
                    "-sw",
                    "-steam",
                    "+dota_launch_custom_game",
                    addonString,
                    "dota"
            );

            //For crash debugging
            File processOutputFile = new File(processOutputFileName);
            File processErrorFile = new File(processErrorFileName);
            File processInputFile = new File(processInputFileName);

            processOutputFile.createNewFile();
            processErrorFile.createNewFile();
            processInputFile.createNewFile();

            pb.redirectOutput(processOutputFile);
            pb.redirectError(processErrorFile);
            pb.redirectInput(processInputFile);

            process = pb.start();

            //Create process monitoring thread
            Context vertxContext = Server.getInstance().getVertx().getOrCreateContext();
            log.info("Creating dota process monitoring thread...");
            log.info("Breezy Server Thread: {}", Thread.currentThread().getName());

            processMonitor = new Thread(()->{
                log.info("Process Monitoring Thread: {}", Thread.currentThread().getName());
                try{
                    log.info("Waiting for dota2.exe to terminate!");
                    process.waitFor();
                    log.info("dota2.exe exited with value: {}", process.exitValue());

                    final int exitValue = process.exitValue();

                    vertxContext.runOnContext(v->{

                        /* Report process as crashed if the exit value isn't 0 or 1
                         * indicating the user closing the process or the process
                         * exiting normally.
                         */
                        if(exitValue != 0 && exitValue != 1){
                            //TODO handle crash
                            Run.handleDotaProcessCrash(exitValue);
                        }

                    });

                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            });

            processMonitor.start();

        }catch (Exception e){
            log.error("Failed to start dota process!");
            log.error(e.getMessage());
            e.printStackTrace();

            //TODO
            throw new FailedToStartDota2("Failed to start dota process!", e);
        }
    }


    public void setDotaPath(String dotaPath) {
        this.dotaPath = dotaPath;
    }



    public void setAddonString(String addonString) {
        this.addonString = addonString;
    }


    public void setProcessOutputFile(String processOutputFile) {
        this.processOutputFileName = processOutputFile;
    }


    public void setProcessErrorFile(String processErrorFile) {
        this.processErrorFileName = processErrorFile;
    }

    public void setProcessInputFile(String processInputFile) {
        this.processInputFileName = processInputFile;
    }
}
