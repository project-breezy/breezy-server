package ca.dal.tpg.model;

import ca.dal.tpg.Server;
import ca.dal.tpg.Store;
import ca.dal.tpg.exceptions.DotaProcessCrash;
import ca.dal.tpg.exceptions.FailedToStartDota2;
import io.vertx.core.Context;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import io.vertx.reactivex.core.Future;
import io.vertx.reactivex.core.Promise;
import io.vertx.reactivex.core.Vertx;
import io.vertx.reactivex.ext.web.Route;
import io.vertx.reactivex.ext.web.Router;
import io.vertx.reactivex.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.File;
import java.io.Serializable;
import java.time.Instant;
import java.util.*;

import static ca.dal.tpg.model.RelayMode.ASYNC;
import static ca.dal.tpg.model.RelayMode.SYNC;


public class Run {

    private static final Logger log = LoggerFactory.getLogger(Run.class);

    //Static
    private static Run activeRun;
    private static Queue<Features> featureQueue =  new LinkedList<>();
    private static Queue<JsonObject> agentResponseQueue = new LinkedList<>();
    private static Promise<JsonObject> agentResponse = null;
    private static Instant agentStartTime = null;

    // Run Info
    private UUID runId;
    private int size; /** The number of times the run should be repeated **/
    private Date startTime;
    private Date endTime;
    private Long duration; //Duration of this run in milliseconds
    private RunStatus status;
    private int progress; /** The number of completed games so far. progress/size = % complete*/
    private String agentName; //Name of the agent that started this run
    private List<Game> gameHistory = new ArrayList<>();  /** A list of the last 10 games associated with this run */
    private RelayMode relayMode = SYNC; //Sync: addon manages dropped frames. Async: Breezy Server managed dropped frames.
    private int timescale = 10; // Host timescale default is 10
    private JsonObject gameConfig;

    private DotaProcess dotaProcess;
    private Game activeGame;
    private RunTelemetry telemetry;
    private Exception error;

    //Events that run objects react to
    private Promise<String> gameStarted; //A promise, that completes when we have a gameId from the dota client.
    private Promise<JsonObject> gameDone; //A promise, that completes when the game client reports the game is over
    private Promise<Void> agentReady; //A promise, that completes when the agent hits a webhook to signal it's ready for the next game
    private Promise<Void> runComplete; //A promise, that completes when the run has completed all its games

    private Route webhook; //The current webhook route to be hit, triggering the next game in a run.

    private boolean waiting; //Flag indicating that the dota 2 client is waiting for a game to be requested.

    /** The value to send to the dota 2 client when it transmits a waiting signal.
     *  For example:
     *      dota 2 client -(sends waiting signal)-> Breezy Server
     *      Breezy Server -(sends clientWaitingResponse)-> dota 2 client
     *
     *  Sending 1 will start a new game.
     *  Sending 0 will tell the dota 2 client to keep waiting.
     */
    private Integer waitingResponse;

    private static long hangDetectionPeriodicId = -1l;

    public static Run fromSQLResult(JsonObject data){

        Run result = new Run();
        result.setSize(data.getInteger("SIZE"));
        result.setStartTime(new Date(data.getLong("START_TIME")));

        if(data.getLong("END_TIME") != null){
            result.setEndTime(new Date(data.getLong("END_TIME")));
        }
        result.setDuration(data.getLong("DURATION"));

        switch(data.getString("RUN_STATUS")){
            case "PENDING":
                result.setStatus(RunStatus.PENDING);
                break;
            case "STARTING":
                result.setStatus(RunStatus.STARTING);
                break;
            case "INPROGRESS":
                result.setStatus(RunStatus.INPROGRESS);
                break;
            case "DONE":
                result.setStatus(RunStatus.DONE);
                break;
            case "CANCELED":
                result.setStatus(RunStatus.CANCELED);
                break;
            case "ERROR":
                result.setStatus(RunStatus.ERROR);
                break;
            case "WAITING":
                result.setStatus(RunStatus.WAITING);
                break;
        }

        result.setProgress(data.getInteger("PROGRESS"));
        result.setAgentName(data.getString("AGENT_NAME"));

        switch (data.getString("RELAY_MODE")){
            case "ASYNC":
                result.setRelayMode(ASYNC);
                break;
            case "SYNC":
                result.setRelayMode(SYNC);
                break;
        }

        result.setTimescale(data.getInteger("TIMESCALE"));

        if (data.containsKey("ID")){
            result.setRunId(UUID.fromString(data.getString("ID")));
        }

        //TODO - load game history
        return result;
    }

    public Run(){
        telemetry = new RunTelemetry();
    }

    public static void getAllRunsHandler(RoutingContext rc){
        JsonArray result = new JsonArray();
        Run.getRuns().onSuccess(
                runs->{
                    runs.forEach(r->result.add(r.toJson()));
                    rc.response().setStatusCode(200).end(result.encode());
                }
        ).onFailure(
                err->rc.response().setStatusCode(500).end(err.getMessage())
        );
    }

    public static void getRunInfoHandler(RoutingContext rc){
        try{
            UUID runId = UUID.fromString(rc.request().getParam("runId"));
            Run.getRun(runId).onSuccess(
                    r->{
                        if(r != null){
                            rc.response().setStatusCode(200).end(
                                    r.toJson().encode()
                            );
                        }else{
                            rc.response().setStatusCode(404).end(
                                    new JsonObject()
                                            .put("error", "Could not find run with specified id! " +
                                                    "Please note that runs are stored in memory, " +
                                                    "and runs from previous instances of the server " +
                                                    "cannot be retrieved!")
                                            .encode()
                            );
                        }
                    }
            );


        }catch (IllegalArgumentException err){
            log.error("Error parsing uuid!");
            log.error(err.getMessage());

            rc.response().setStatusCode(500).end(
                    new JsonObject()
                            .put("error", "Malformed runId!")
                            .encode()
            );
        }
    }

    public static void activeRunInfoHandler(RoutingContext rc){

        if(Run.activeRun != null){
            rc.response().setStatusCode(200).end(Run.activeRun.toJson().encode());
            log.info(Run.activeRun.toJson().encodePrettily());
        }else{
            rc.response().setStatusCode(404).end(
                    new JsonObject()
                            .put("error", "there is no active run!")
                            .encode()
            );
        }
    }

    public static void cancelActiveRunHandler(RoutingContext rc){
        Run r =  Run.cancelActiveRun();
        if(r != null){
            rc.response().setStatusCode(200).end(
                    r.toJson().encode()
            );
        }else{
            rc.response().setStatusCode(500).end(
                    new JsonObject()
                            .put("error", "No active run! Cannot cancel nothing!")
                            .encode()
            );
        }
    }

    /** Cancels the active run if one exists.
     *
     * @return the canceled run or null if there was no active run.
     */
    public static Run cancelActiveRun(){
        if(activeRun != null){

            activeRun.cancel();
            Run result = activeRun;
            activeRun = null;

            Server.getInstance().v().cancelTimer(hangDetectionPeriodicId);

            //Notify agent of canceled run...
            Server.getInstance().getAgentClient().sendGameEndNotification(result.toJson()).future()
                    .onSuccess(
                        notificationAcknowledged->{
                            log.info("Agent ({}) notified of canceled run.");
                        })
                    .onFailure(err->log.error("Error notifying agent of canceled run! Is the run.update.route implemented properly on the agent?")
                    );

            return result;

        }
        return null;
    }

    public static void createRun(RoutingContext rc){
        //Prevent double starting a run TODO - multi slot runs
        if(activeRun != null){
            rc.response().setStatusCode(500).end(
                    new JsonObject()
                            .put("error",
                                    "Cannot start run, there already is an active run! " +
                                    "Please cancel or wait for the active run to complete before starting a new one!")
                            .encode()
            );
        }else{
            log.info("Attempting to create run!");
            try{

                JsonObject runInfo = rc.getBodyAsJson();
                Run r = new Run();
                r.setRunId(UUID.randomUUID());
                r.setSize(runInfo.getInteger("size"));
                r.setStartTime(Date.from(Instant.now()));
                r.setStatus(RunStatus.PENDING);
                r.setProgress(0);
                r.setAgentName(runInfo.getString("agent"));

                if(runInfo.containsKey("gameConfig")){
                    r.setGameConfig(runInfo.getJsonObject("gameConfig"));
                }else{
                    r.setGameConfig(new JsonObject().put("agentHero", "npc_dota_hero_nevermore"));
                }


                //Optional timescale
                if(runInfo.containsKey("timescale")){
                    r.setTimescale(runInfo.getInteger("timescale"));
                }

                //Optional relayMode
                if(runInfo.containsKey("relayMode")){
                    switch (runInfo.getString("relayMode").toLowerCase()){
                        case "async":
                            r.setRelayMode(ASYNC);
                            break;
                        case "sync":
                            r.setRelayMode(SYNC);
                            break;
                    }
                }


                log.info("run: {}", r.toJson().encodePrettily());

                rc.response().setStatusCode(200).end(r.toJson().encode());

                Server.getInstance().getServerState().onSuccess(
                        serverState->Server.getInstance().eb().publish(
                                "sentry.latency", serverState)
                        );

                r.start();
            }catch (Exception e){
                log.error(e.getMessage());
                e.printStackTrace();
            }
        }
    }

    /* Async Logic */

    private static void handleAgentResponse(JsonObject response){
        log.info("Got agent response!");
        log.info("{}", response.encodePrettily());
        agentResponseQueue.add(response);

        if(activeRun == null){ //Do nothing if there is no run
            return;
        }
        //TODO ActionArrays
        activeRun.telemetry().addAgentRelayDuration(
                Instant.now().toEpochMilli()-agentStartTime.toEpochMilli(),
                -1
        );

      /* Poll our queue until we have the most recent feature set.
       * Then send that feature set to the agent.
       */
        log.info("agentResponseQueueSize: {}", agentResponseQueue.size());
        log.info("featureQueueSize: {}", featureQueue.size());

        processFeatureQueue();

        if(agentResponse == null || agentResponse.future().isComplete()){
            agentResponse = null;
        }

    }

    private static void processFeatureQueue(){
        while (featureQueue.size() > 0){
            activeRun.telemetry().addDropFrames(featureQueue.size()-1);
            if(featureQueue.size() == 1){
                Features toSend = featureQueue.poll();
                log.info("Sending {} to agent", toSend.getSequenceNumber());
                agentStartTime = Instant.now();
                agentResponse =
                        Server
                                .getInstance()
                                .getAgentClient()
                                .relay(toSend);
                agentResponse.future().onSuccess(Run::handleAgentResponse);
                break;
            }
            if(featureQueue.size() > 1){
                log.info("Dropping {}", featureQueue.poll().getSequenceNumber());
            }
        }
    }

    public static void dota2AsyncGameHandler(RoutingContext rc){
        Instant iStart = Instant.now();
        log.info("Feature Queue size: {}", featureQueue.size());
        log.info("Agent Response Queue size: {}", agentResponseQueue.size());

        Features features = (Features)rc.data().get("features");

        //Add this feature set to the queue
        featureQueue.add(features);

        //If we haven't sent the first feature set yet
        if(agentResponse == null){
            agentStartTime = Instant.now();
            agentResponse = Server
                    .getInstance()
                    .getAgentClient()
                    .relay(features);
            agentResponse.future()
                    .onSuccess(Run::handleAgentResponse);
        }

        if(agentResponseQueue.size() > 0){
            while (agentResponseQueue.size() > 0){
                activeRun.telemetry().addDropFrames(agentResponseQueue.size()-1);
                if(agentResponseQueue.size() == 1){
                    JsonObject response = agentResponseQueue.poll();
                    log.info("Sending {} to dota game client", response.getInteger("sequenceNo") );
                    response.put("_sequenceNo", features.getSequenceNumber());
                    rc.response().setStatusCode(200).rxEnd(
                            response.encode()
                    ).subscribe(
                            ()->{
                                long breezyResponseTime = Instant.now().toEpochMilli() - iStart.toEpochMilli();
                                activeRun.telemetry().addBreezyResponseTime(breezyResponseTime);

                            }
                    );
                }
                if(agentResponseQueue.size() > 1){
                    log.info("Dropping {} on response", agentResponseQueue.poll().getInteger("sequenceNo"));
                }

            }

        }else{
            log.info("Sending DO NOTHING to dota game client!");
            rc.response().setStatusCode(200).rxEnd(
                    new JsonObject().put("_discard", true ).encode()
            ).subscribe(
                    ()->{
                        long breezyResponseTime = Instant.now().toEpochMilli() - iStart.toEpochMilli();
                        activeRun.telemetry().addBreezyResponseTime(breezyResponseTime);

                    }
            );
        }

        //Compute and send telemetry
        activeRun.telemetry().addFeatureVector(features.getSourceArray());
        Server.getInstance().getServerState()
                .onSuccess(
                        serverState->Server.getInstance().eb().publish("sentry.latency", serverState));
        log.info(activeRun.telemetry().toJson().encodePrettily());
    }



    /* Sync Logic */

    /** Handle dota 2 client interactions during a game.
     * Note: These requests are sent to us from the Server's game handler.
     * @param rc Routing context of the request
     */
    public static void dota2GameHandler(RoutingContext rc){
        Instant iStart = Instant.now();

        Features features = (Features)rc.data().get("features");

        Server.getInstance().configRetriever().rxGetConfig().subscribe(
                config->{
                    if(!config.getBoolean("overrideAgent")){
                        Date startRelayTime = Date.from(Instant.now());
                        log.info("Relaying feature vector to agent!");
                        Server.getInstance().getAgentClient().relay(features).future().onSuccess(
                                response->{
                                    //Compute Agent Latency
                                    long relayDuration = Date.from(Instant.now()).getTime() - startRelayTime.getTime();
                                    //Store it
                                    //TODO ActionArrays
                                    activeRun.telemetry().addAgentRelayDuration(relayDuration,
                                            -1);

                                    response.put("_sequenceNo", features.getSequenceNumber());


                                    //log.info("Got action code {} from agent after {}ms, sending to dota client!", String.valueOf(response.getInteger("actionCode")), relayDuration);
                                    //Send response back to dota 2 client
                                    rc.response().rxEnd(response.encode())
                                            .subscribe(
                                                    ()->{
                                                        long breezyResponseTime = Instant.now().toEpochMilli()-iStart.toEpochMilli();
                                                        activeRun.telemetry().addBreezyResponseTime(breezyResponseTime);
                                                    }
                                            );
                                }
                        ).onFailure(
                                err->{
                                    log.error("Error relaying feature array to agent.");
                                    log.error(err.getMessage());
                                    err.printStackTrace();
                                }
                        );
                    }else{

                        //Build mock action [1, 45, 800, 1, 1]
                        JsonArray actionArray = new JsonArray().add(1).add(45).add(800).add(1).add(1);

                        rc.response().rxEnd(new JsonObject().put("actionCode", actionArray).put("_sequenceNo", features.getSequenceNumber()).encode()).subscribe(
                                ()->{
                                    Instant iEnd = Instant.now();
                                    log.info("Server response time: {}ms", iEnd.toEpochMilli()-iStart.toEpochMilli());
                                    activeRun.telemetry().addBreezyResponseTime(iEnd.toEpochMilli()-iStart.toEpochMilli());
                                }
                        );
                    }
                }
        );



        //Compute and send telemetry
        activeRun.telemetry().addFeatureVector(features.getSourceArray());
        Server.getInstance().getServerState()
                .onSuccess(
                        serverState->Server.getInstance().eb().publish("sentry.latency", serverState));
        log.info(activeRun.telemetry().toJson().encodePrettily());
    }

    /** Handle client interaction at the start, between and end of games.
     *
     * @param rc
     */
    public static void dota2ClientHandler(RoutingContext rc){
        JsonObject gameInfo = rc.getBodyAsJson();
        String gameId = gameInfo.getString("gameId");

        if(gameInfo.containsKey("state")){
            switch (gameInfo.getString("state")){
                case "start":

                    /* Send back relay mode and timescale to use
                     * sent in  '<relay mode>|<timescale>' format
                     */
                    String responseString = activeRun.getRelayMode().name() + "|" + activeRun.getTimescale();
                    log.info("Game start msg recieved, responding with: {}", responseString);
                    rc.response().setStatusCode(200).end(
                        responseString
                    );
                    activeRun.gameStarted.complete(gameId);
                    break;
                case "end":
                    //Complete active game
                    activeRun.gameDone.tryComplete(gameInfo);
                    rc.response().setStatusCode(200).end();
                    //Reset feature and agent response queues between games.
                    agentResponseQueue = new LinkedList<>();
                    featureQueue = new LinkedList<>();
                    break;
                case "waiting":
                    if(!activeRun.waiting){
                        activeRun.waiting = true;
                    }
                    rc.response().setStatusCode(200).end(activeRun.waitingResponse.toString()); //Send wait or start signal to game client
                    //Reset the waitingResponse if we just told the client to start a game
                    if(activeRun.waitingResponse == 1){
                        activeRun.waitingResponse = 0;
                    }
                    break;
            }
        }
    }



    /** Retrieves all runs saved to the database.
     *
     * @return a list of all runs on this server
     */
    public static Future<List<Run>> getRuns(){
        return Store.getInstance().getRuns();
    }

    /** Retrieves a run by id.

     * @param id UUID of the run to be retrieved.
     * @return the run with the specified id or null if it could not be found.
     */
    public static Future<Run> getRun(UUID id){
        if(activeRun != null && activeRun.getRunId().equals(id)){
            Promise<Run> promise = Promise.promise();
            promise.complete(activeRun);
            return promise.future();
        }

        return Store.getInstance().getRun(id);
    }

    /** Called by the dota process monitoring thread to throw
     *  and exception and let us handle the situation.
     * @param exitValue
     */
    public static void handleDotaProcessCrash(int exitValue){
        if(activeRun != null){
            log.info("DotaProcessCrash Handler invoked!");
            activeRun.gameDone.tryFail(new DotaProcessCrash(exitValue));
        }
    }

    public void start(){
        if(activeRun != null){
            log.info("Overwriting active run!! This probably shouldn't happen");
        }
        activeRun = this;

        //Create a new game
        setActiveGame(new Game(this.getRunId(), getGameConfig()));
        log.info(toJson().encodePrettily());
        log.info("Run waiting: {}", waiting);

        //If the dota process exists and is alive, the client is waiting
        if(getDotaProcess() != null && getDotaProcess().isAlive()){
            setWaitingResponse(1); //Send signal to start the game
        }else{
            //Otherwise we need to start a new dota process
            Server.getInstance().configRetriever().rxGetConfig().subscribe(
                    config->{
                        try{
                            DotaProcess dota = new DotaProcess();
                            dota.setDotaPath(config.getString("dotaBetaFolderPath"));
                            dota.setAddonString(config.getString("workshop_addon_id"));
                            dota.setProcessOutputFile("dotaOutput.log");
                            dota.setProcessInputFile("dotaInput.log");
                            dota.setProcessErrorFile("dotaError.log");
                            dota.createDotaProcess();
                            setDotaProcess(dota);
                        }catch (FailedToStartDota2 e){
                            setError(e.getCause());
                            setStatus(RunStatus.ERROR);
                            log.error("Affected Run: {}", toJson().encodePrettily());
                        }
                    }
            );
        }

        log.info("Starting run {}", getRunId().toString());
        setStatus(RunStatus.STARTING);
        notifySentry();

        //Set up game started promise
        gameStarted = Promise.promise();
        gameStarted.future().onSuccess(this::gameStartedHandler);

        //Set up game end promise
        gameDone = Promise.promise();
        gameDone.future()
                .onSuccess(this::gameDoneHandler)
                .onFailure(this::gameErrorHandler)
        ;

        //Set up completion handler
        runComplete = Promise.promise();
        runComplete.future().onSuccess(this::completeHandler);


    }

    //Called if gameDone fails
    private void gameErrorHandler(Throwable err){
        log.info("Got game failure/crash!");

        //Cancel hang detection
        Server.getInstance().v().cancelTimer(hangDetectionPeriodicId);

        //Update our status and clear the active game
        activeGame.setStatus(GameStatus.CRASHED);
        setStatus(RunStatus.ERROR);
        Store.getInstance().insert(activeGame);
        addGameToHistory(activeGame);
        activeGame = null;

        //Increase the size of the run to compensate
        log.info("Adding an extra game to make up for the lost game");
        size += 1;
        dotaProcess = null; //Reset the crashed process

        //Handle additional details if we understand the crash type.
        if (err instanceof DotaProcessCrash){
            DotaProcessCrash dotaCrashErr = (DotaProcessCrash)err;
            setCrashInfo(dotaCrashErr.getExitValue());
        }

        //Notify Sentry of the terrible news
        notifySentry();

        /* Because we just increased our size,
         * we will always have at least one more game to do.
         * So let's get ready to do that.
         */

        //If we're bypassing the agent, just start
        Server.getInstance().configRetriever().rxGetConfig().subscribe(
                config->{
                    if(config.getBoolean("bypass.agent.ready.between.games")){
                        start();
                    }else{
                        //Otherwise
                        //Generate this run's webhook, and start the run when the agentReady promise completes.
                        generateWebhook();
                        agentReady.future().onSuccess(
                                ready->this.start()
                        );
                        setStatus(RunStatus.WAITING);

                        log.info("{}", toJson().encodePrettily());

                        //Notify Sentry
                        notifySentry();

                        //Notify agent
                        notifyAgentOfGameEnd();
                    }
                }
        );


    }

    private void gameStartedHandler(String gameId){
        getActiveGame().setGameId(gameId);
        getActiveGame().setStatus(GameStatus.RUNNING);
        setStatus(RunStatus.INPROGRESS);
        notifySentry();
    }

    /** Handles the logistics that should take place after a game of dota
     *  completes. If necessary, sets up a new game otherwise completes
     *  the run. Notifies Sentry as appropriate, notifies Agent as configured.
     */
    private void gameDoneHandler(JsonObject gameInfo){
        //Set the results for our active game
        getActiveGame().setWinner(gameInfo.getString("winner"));
        getActiveGame().setDireKills(Integer.parseInt(gameInfo.getString("direKills")));
        getActiveGame().setRadiantKills(Integer.parseInt(gameInfo.getString("radiantKills")));
        getActiveGame().setDeaths(Integer.parseInt(gameInfo.getString("deaths")));


        log.info("A game has completed!");
        addGameToHistory(getActiveGame());
        getActiveGame().setStatus(GameStatus.COMPLETE);
        Store.getInstance().insert(getActiveGame());
        setActiveGame(null);
        progress++;

        if(getSize() > getProgress()){
            log.info("We have more games to do..");

            Server.getInstance().configRetriever().rxGetConfig().subscribe(
                    config->{
                        if(config.getBoolean("bypass.agent.ready.between.games")){
                            //If we're bypassing the agent, just start
                            this.start();
                        }else{
                            //Otherwise
                            //Generate this run's webhook, and start the run when the agentReady promise completes.
                            generateWebhook().onComplete(
                                    webhookReady->{
                                        agentReady.future().onSuccess(
                                                ready->this.start()
                                        );
                                        setStatus(RunStatus.WAITING);

                                        log.info("{}", toJson().encodePrettily());

                                        //Notify Sentry
                                        notifySentry();

                                        //Notify agent
                                        notifyAgentOfGameEnd();
                                    }
                            );

                        }
                    }
            );


        }else{
            getRunComplete().complete();
        }
    }


    /** Handles the logistics of finishing up a run once all its games have been completed.
     *  - Updates
     *      - status
     *      - duration
     *      - end time
     *  - Adds the run to the list of completed runs
     *  - Clears the active run
     *  - Notifies sentry
     *  - destroys dota process
     *  - Notifies agent
     *
     */
    private Future<Run> completeHandler(Void aVoid){
        Promise<Run> completedRun = Promise.promise();
        log.info("All games done! Finalizing run!");

        //Update instance variables
        setEndTime(Date.from(Instant.now()));
        setDuration(getEndTime().getTime() - getStartTime().getTime());
        setStatus(RunStatus.DONE);

        //Clean up dota process
        getDotaProcess().destroyDotaProcess();
        setDotaProcess(null);

        //Notify sentry
        notifySentry();

        //If configured, notify agent
        Server.getInstance().configRetriever().rxGetConfig().subscribe(
                config->{
                    if(!config.getBoolean("bypass.agent.ready.between.games")){
                        notifyAgentOfGameEnd();
                    }
                }
        );

        //Terminate hang detection
        Server.getInstance().v().cancelTimer(hangDetectionPeriodicId);

        //Insert the completed run into the database
        Store.getInstance().insert(this);
        //Clear the active run
        Run.activeRun = null;

        //Pass myself back
        completedRun.complete(this);
        return completedRun.future();
    }

    private void notifySentry(){
        Server.getInstance().eb().publish("sentry.activeRun", toJson());
    }

    private void notifyAgentOfGameEnd(){
        Server.getInstance().getAgentClient().sendGameEndNotification(toJson()).future()
                .onSuccess(
                        notificationAcknowledged->{
                            log.info("Run successfully notified agent ({}) of game end!", getAgentName() );
                            log.info("Response from agent server: {}", notificationAcknowledged.encodePrettily());
                            if(getWebhook() != null){
                                log.info("Awaiting webhook hit...");
                            }

                        }
                ).onFailure(
                        err->{
                            log.error("Error notifying agent ({}) of game end! Is the run.update.route implemented properly on the agent?", getAgentName());
                            log.error(err.getMessage());
                        }
        );
    }

    public JsonObject toJson(){
        JsonObject result = new JsonObject()
                .put("id", runId.toString());

        if(activeGame != null && activeGame.getGameId() != null){
            result.put("activeGameId", activeGame.getGameId());
        }

        result
            .put("size", size)
            .put("startTime", startTime.toString());

        if(endTime != null){
            result.put("endTime", endTime.toString());
        }
        result
            .put("duration", getDuration())
            .put("status", status.name())
            .put("progress",  progress);

        JsonArray historyArray = new JsonArray();

        gameHistory.forEach(g->{
            historyArray.add(g.getGameId());
        });

        result.put("gameIds", historyArray);

        if(error != null){
            result.put("error", error.getMessage());
        }

        if(webhook != null){
            result.put("webhook", webhook.getPath());
        }

        result.put("timescale", getTimescale());
        result.put("relayMode", getRelayMode().name());

        /* Add game details to json,
           If we have an active game we report its details.
           If we don't have an active game, we report our last game's details if that exists.
         */

        Game game = activeGame != null? activeGame : gameHistory.size() > 0 ? gameHistory.get(gameHistory.size()-1):null;

        if (game != null){
            if(game.getWinner() != null){
                result.put("winner", game.getWinner());
            }

            if(game.getDireKills() != null){
                result.put("direKills", game.getDireKills());
            }

            if(game.getRadiantKills() != null){
                result.put("radiantKills", game.getRadiantKills());
            }

            if(game.getDeaths() != null){
                result.put("deaths", game.getDeaths());
            }

            if(game.getAgentHero() != null){
                result.put("gameConfig", getGameConfig());
            }
        }


        JsonArray crashArray = new JsonArray();
        gameHistory.stream()
                .filter(g->g.getStatus().equals(GameStatus.CRASHED))
                .forEach(g->crashArray.add(g.getGameId()));

        if(crashArray.size() > 0){
            result.put("crashedGames", crashArray);
        }

        return result;
    }

    /** Returns the duration of the run. If the run has not yet completed
     *  (ie: there is no endTime and therefor no value set as the final
     *  duration. Then the active run duration is calculated by subtracting
     *  the current time from the start time.
     *
     * @return The duration of this run in milliseconds.
     */
    public long getDuration(){
        if(duration != null){
            return duration;
        }else {
            return Date.from(Instant.now()).getTime() - startTime.getTime();
        }
    }

    /** Creates a webhook route, which the agent can call to start the next game
     *  in a run.
     */
    public Future<Void> generateWebhook(){
        Promise<Void> promise = Promise.promise();
        Server.getInstance().configRetriever().rxGetConfig().subscribe(
                config->{
                    agentReady = Promise.promise();

                    webhook =  Server.getInstance().router().route(HttpMethod.GET,
                            config.getString("run.webhook.prefix") +
                                    getRunId().toString() +
                                    config.getString("run.webhook.suffix")
                    );

                    webhook.handler(rc->{
                        log.info("Webhook invoked by agent ({})!", agentName);
                        agentReady.complete();
                        status = RunStatus.STARTING;
                        webhook.remove(); //Remove the route after we're done to prevent strange behavior/double invokes.
                        webhook = null;
                        rc.response().setStatusCode(200).end(toJson().encode());
                    });

                    promise.complete();
                }
        );
        return promise.future();
    }

    //Set some specific errors for game crashes we know about...
    private void setCrashInfo(int exitValue){
        if( exitValue == -1073741819){
            Exception e = new Exception("Suspected accessViolation has occured crashing game ("+activeGame.getGameId()+"), dota2.exe crashed with exit value " + exitValue);
            setError(e);
            activeGame.setError(e);
        }else{
            Exception e = new Exception("Unexpected error caused the game ("+activeGame.getGameId()+") to crash with exit value " + exitValue);
            setError(e);
            activeGame.setError(e);
        }
    }

    //Called if cancel run route is invoked
    public void cancel(){
        activeGame = null;
        endTime = Date.from(Instant.now());
        duration = getEndTime().getTime() - getStartTime().getTime();
        status = RunStatus.CANCELED;

        dotaProcess.destroyDotaProcess();
        Store.getInstance().insert(this);

    }


    public void setError(Exception error) {
        this.error = error;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public UUID getRunId() {
        return runId;
    }

    public void setRunId(UUID runId) {
        this.runId = runId;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    private void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public void setStatus(RunStatus status) {
        this.status = status;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public RunTelemetry telemetry(){
        return telemetry;
    }

    private void setDuration(Long duration) {
        this.duration = duration;
    }

    public DotaProcess getDotaProcess() {
        return dotaProcess;
    }

    private void setDotaProcess(DotaProcess dotaProcess) {
        this.dotaProcess = dotaProcess;
        Server.getInstance().v().setPeriodic(10000, hangDetection->{
            hangDetectionPeriodicId = hangDetection;
            Server.getInstance().configRetriever().rxGetConfig().subscribe(
                    config->{
                        if(telemetry.lastFeatureVector != null){
                            log.info("Time since last feature vector: {}ms", Instant.now().toEpochMilli() - telemetry.lastFeatureVector);
                        }else{
                            log.info("No feature vector yet!");
                        }
                        if (getActiveGame() == null && telemetry != null && telemetry.lastFeatureVector != null && Instant.now().toEpochMilli() - telemetry.lastFeatureVector > 60000){
                            log.info("No active game and no feature vector for over 1 min, cancelling hangDetection!");
                            Server.getInstance().v().cancelTimer(hangDetection);
                        }
                        if(getActiveGame() != null && //If a game exists for this run
                                getActiveGame().getStatus() == GameStatus.RUNNING && //And the game is in the RUNNING STATE
                                telemetry.lastFeatureVector != null && // and we've received at least one feature vector
                                //TODO subsequent game start can get interrupted
                                Instant.now().toEpochMilli() - telemetry.lastFeatureVector > config.getLong("hangThreshold")){ //and we haven't received anything else for 30 seconds

                            log.info("Possible game hang detected, passed hangThreshold of {}ms", config.getLong("hangThreshold"));

                            getActiveGame().setStatus(GameStatus.RESTARTING); //Set game status to restarting
                            log.info("GameStatus: {}",getActiveGame().getStatus());
                            try {
                                dotaProcess.restartDotaProcess();
                                getActiveGame().setStatus(GameStatus.STARTING);
                                log.info("GameStatus: {}",getActiveGame().getStatus());

                                //Reset game started promise && lastFeatureVector
                                telemetry.lastFeatureVector = null;
                                gameStarted = Promise.promise();
                                gameStarted.future().onSuccess(this::gameStartedHandler);
                            } catch (FailedToStartDota2 failedToStartDota2) {
                                log.error("Error restarting dota 2 process after possible hang! {}", Date.from(Instant.now()).toString());
                            }


                        }
                    }
            );

        });
    }

    private Promise<Void> getRunComplete() {
        return runComplete;
    }


    public Game getActiveGame() {
        return activeGame;
    }

    private void setActiveGame(Game activeGame) {
        this.activeGame = activeGame;
    }

    private List<Game> getGameHistory() {
        return gameHistory;
    }

    public String getAgentName() {
        return agentName;
    }

    private Route getWebhook() {
        return webhook;
    }

    private void setWaitingResponse(Integer waitingResponse) {
        this.waitingResponse = waitingResponse;
    }

    public static Run getActiveRun(){
        return activeRun;
    }

    public RunStatus getStatus() {
        return status;
    }

    private void addGameToHistory(Game g){
        gameHistory.add(g);
        if(gameHistory.size() > 10){
            gameHistory.remove(0);
        }
    }

    public RelayMode getRelayMode() {
        return relayMode;
    }

    public void setRelayMode(RelayMode relayMode) {
        this.relayMode = relayMode;
        switch (this.relayMode){
            case SYNC:
                Server.getInstance().useSyncGameHandler();
                break;
            case ASYNC:
                Server.getInstance().useAsyncGameHandler();
                agentResponse = null; //Clear any promises from past runs
                break;
        }
    }

    public int getTimescale() {
        return timescale;
    }

    public void setTimescale(int timescale) {
        this.timescale = timescale;
    }

    public JsonObject getGameConfig() {
        return gameConfig;
    }

    public void setGameConfig(JsonObject gameConfig) {
        this.gameConfig = gameConfig;
    }
}
