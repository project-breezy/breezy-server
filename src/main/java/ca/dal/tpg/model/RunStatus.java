package ca.dal.tpg.model;

public enum RunStatus {
    PENDING,STARTING,INPROGRESS,DONE,CANCELED,ERROR,WAITING
}
