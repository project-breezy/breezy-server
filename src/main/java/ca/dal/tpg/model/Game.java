package ca.dal.tpg.model;

import ca.dal.tpg.exceptions.LoadedException;
import io.vertx.core.json.JsonObject;

import java.util.UUID;

/** @Author Alexandru Ianta
 *  The Game class represents a game of dota 2 executed as part of a run.
 */
public class Game {

    //Meta
    private String gameId;
    private GameStatus status = GameStatus.STARTING;
    private Exception error = null;
    private UUID parent;
    private String agentHero = "npc_dota_hero_nevermore";

    //Game stats
    private String winner = null;
    private Integer direKills = null;
    private Integer radiantKills = null;
    private Integer deaths = null;

    public static Game fromSQLResult(JsonObject data){
        Game result = new Game(UUID.fromString(data.getString("RUN_ID")));
        result.setDeaths(data.getInteger("DEATHS"));
        result.setRadiantKills(data.getInteger("RADIANT_KILLS"));
        result.setDireKills(data.getInteger("DIRE_KILLS"));
        result.setWinner(data.getString("WINNER"));
        result.setGameId(data.getString("ID"));
        result.setError(data.getString("ERROR") != null?
                new LoadedException(data.getString("ERROR")):
                null);

        switch (data.getString("STATUS")){
            case "STARTING":
                result.setStatus(GameStatus.STARTING);
                break;
            case "RUNNING":
                result.setStatus(GameStatus.RUNNING);
                break;
            case "CRASHED":
                result.setStatus(GameStatus.CRASHED);
                break;
            case "COMPLETE":
                result.setStatus(GameStatus.COMPLETE);
                break;
        }

        return result;
    }


    public Game(UUID parentRunId, JsonObject gameConfig){
        this.parent = parentRunId;
        if(gameConfig != null && gameConfig.containsKey("agentHero")){
            this.agentHero = gameConfig.getString("agentHero");
        }

    }

    public Game(UUID parentRunId){

        this.parent = parentRunId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }

    public Exception getError() {
        return error;
    }

    public void setError(Exception error) {
        this.error = error;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public Integer getDireKills() {
        return direKills;
    }

    public void setDireKills(Integer direKills) {
        this.direKills = direKills;
    }

    public Integer getRadiantKills() {
        return radiantKills;
    }

    public void setRadiantKills(Integer radiantKills) {
        this.radiantKills = radiantKills;
    }

    public Integer getDeaths() {
        return deaths;
    }

    public void setDeaths(Integer deaths) {
        this.deaths = deaths;
    }

    public UUID getParentId() {
        return parent;
    }

    public void setParentId(UUID id){
        this.parent = id;
    }

    public String getAgentHero() {
        return agentHero;
    }

    public void setAgentHero(String agentHero) {
        this.agentHero = agentHero;
    }
}
