package ca.dal.tpg.model;

import ca.dal.tpg.Server;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.Date;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RunTelemetry {

    private final int AGENT_LATENCY_LOG_SIZE = 60;
    private final int DOTA_CLIENT_LATENCY_LOG_SIZE = 60;
    private final int BREEZY_SERVER_RESPONSE_LOG_SIZE = 60;
    private final int DROPPED_FRAMES_LOG_SIZE = 60;
    private final int ADDON_RTT_LOG_SIZE = 60;

    private List<Long> dotaClientLatency = new ArrayList<>();
    private List<Long> breezyResponseTime = new ArrayList<>();
    private List<Long> agentLatency = new ArrayList<>();
    private List<Integer> droppedFrames = new ArrayList<>();
    private List<JsonArray> featureBuffer = new ArrayList<>();
    private List<Double> addonRtt = new ArrayList<>();
    private Integer lastActionCode = -1;

    public Long lastFeatureVector; //Timestamp when the last feature vector was received

    public void addAddonRtt(Double d){
        addonRtt.add(d);
        if(addonRtt.size() > ADDON_RTT_LOG_SIZE){
            addonRtt.remove(0);
        }
    }

    public void addFeatureVector(JsonArray vector){

        //Update telemetry
        if(lastFeatureVector == null){
            lastFeatureVector = Date.from(Instant.now()).getTime();
        }else{
            long current = Date.from(Instant.now()).getTime();
            dotaClientLatency.add(current-lastFeatureVector);
            lastFeatureVector = current;
        }

        //Prune dota client latency log as required
        if(dotaClientLatency.size() > DOTA_CLIENT_LATENCY_LOG_SIZE){
            dotaClientLatency.remove(0);
        }

        //Add vector to buffer
        featureBuffer.add(vector);
    }

    public JsonArray readFeatureVector(){
        return featureBuffer.size()>0 ? featureBuffer.remove(0) : null;
    }

    public void addDropFrames(int frames){
        droppedFrames.add(frames);

        if(droppedFrames.size() > DROPPED_FRAMES_LOG_SIZE){
            droppedFrames.remove(0);
        }
    }

    public void addAgentRelayDuration(Long duration, Integer actionCode){
        lastActionCode = actionCode;
        agentLatency.add(duration);

        //Manage agent latency records
        //Keep 60 records
        if(agentLatency.size() > AGENT_LATENCY_LOG_SIZE){
            agentLatency.remove(0);
        }
    }

    public void addBreezyResponseTime(long duration){

        Server.getInstance().configRetriever().rxGetConfig().subscribe(
                config->{
                    if(config.getBoolean("overrideAgent")){
                        lastActionCode = Server.getInstance().getActionCode();
                    }
                }
        );

        breezyResponseTime.add(duration);

        if(breezyResponseTime.size() > BREEZY_SERVER_RESPONSE_LOG_SIZE){
            breezyResponseTime.remove(0);
        }
    }

    public JsonObject toJson(){
        JsonObject result = new JsonObject();

        JsonArray dotaLatencyData = new JsonArray();
        JsonArray agentLatencyData = new JsonArray();
        JsonArray breezyResponseData = new JsonArray();
        JsonArray droppedFramesResponseData = new JsonArray();

        Double agentAverage = 0.0;
        long agentMax = 0L;
        long agentMin = 0L;

        Double dotaAverage = 0.0;
        long dotaMax = 0L;
        long dotaMin = 0L;

        Double breezyAverage = 0.0;
        long breezyMax = 0L;
        long breezyMin = 0L;

        Double framesAverage = 0.0;
        int minFrames = 0;
        int maxFrames = 0;

        Double addonRttAverage = 0.0;
        double minAddonRtt = 0.0;
        double maxAddonRtt = 0.0;

        if(agentLatency.size() > 0){
            agentMax = Collections.max(agentLatency);
            agentMin = Collections.min(agentLatency);

            agentAverage = agentLatency
                    .stream()
                    .peek(l->agentLatencyData.add(l))
                    .mapToDouble(l->l)
                    .average()
                    .orElse(0.0);
        }

        if(dotaClientLatency.size() > 0){
            dotaMax = Collections.max(dotaClientLatency);
            dotaMin = Collections.min(dotaClientLatency);

            dotaAverage = dotaClientLatency
                    .stream()
                    .peek(l->dotaLatencyData.add(l))
                    .mapToDouble(l->l)
                    .average()
                    .orElse(0.0);
        }

        if(breezyResponseTime.size()>0){
            breezyMax = Collections.max(breezyResponseTime);
            breezyMin = Collections.min(breezyResponseTime);

            breezyAverage = breezyResponseTime
                    .stream()
                    .peek(l->breezyResponseData.add(l))
                    .mapToDouble(l->l)
                    .average()
                    .orElse(0.0);
        }

        if(droppedFrames.size()>0){
            minFrames = Collections.min(droppedFrames);
            maxFrames = Collections.max(droppedFrames);
            framesAverage = droppedFrames
                    .stream()
                    .peek(i->droppedFramesResponseData.add(i))
                    .mapToDouble(i->i)
                    .average()
                    .orElse(0.0)
            ;
        }

        if(addonRtt.size() > 0){
            minAddonRtt = Collections.min(addonRtt);
            maxAddonRtt = Collections.max(addonRtt);
            addonRttAverage = addonRtt
                    .stream()
                    .mapToDouble(d->d)
                    .average()
                    .orElse(0.0);
        }

        result
                .put("averageClientLatency", dotaAverage)
                .put("maxClientLatency", dotaMax)
                .put("minClientLatency", dotaMin)
                .put("minAddonRtt", minAddonRtt)
                .put("maxAddonRtt", maxAddonRtt)
                .put("averageAddonRtt", addonRttAverage)
                .put("averageAgentLatency", agentAverage)
                .put("maxAgentLatency", agentMax)
                .put("minAgentLatency", agentMin)
                .put("averageBreezyResponse", breezyAverage)
                .put("minBreezyResponse", breezyMin)
                .put("maxBreezyResponse", breezyMax)

                .put("clientLatencyData", dotaClientLatency.size() > 0?dotaClientLatency.get(dotaClientLatency.size()-1):0L)
                .put("agentLatencyData", agentLatency.size()>0?agentLatency.get(agentLatency.size()-1):0L)
                .put("breezyResponseTime", breezyResponseTime.size()>0?breezyResponseTime.get(breezyResponseTime.size()-1):0L)
                .put("addonRtt", addonRtt.size() > 0 ?addonRtt.get(addonRtt.size()-1):0)
                .put("lastActionCode", lastActionCode)
        ;

        if(Run.getActiveRun().getRelayMode().equals(RelayMode.ASYNC)){
            result.put("minDroppedFrames", minFrames)
                    .put("maxDroppedFrames", maxFrames)
                    .put("averageDroppedFrames", framesAverage)
                    .put("droppedFramesData", droppedFramesResponseData.size() > 0?droppedFrames.get(droppedFrames.size()-1):0);
        }

        return result;
    }

}
