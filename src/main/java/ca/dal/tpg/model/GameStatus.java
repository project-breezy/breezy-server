package ca.dal.tpg.model;

public enum GameStatus {
    STARTING,RUNNING,CRASHED,COMPLETE,RESTARTING
}
